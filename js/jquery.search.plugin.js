/**
 * search jQuery plugin.
 *
 * @category example
 * @author Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @license http://creativecommons.org/licenses/by-nc-nd/3.0/deed.hu Creative Commons
 */
(function ($) {
    $.fn.search = function (aData) {
        /**
         * Search box id.
         * @type {String}
         */
        var searchBoxId = this.attr('id') + '__BOX',
            /**
             * Search box child.
             * @type {Array}
             */
             searchBoxChild = null;

        /**
         * Returns search box children.
         * @return {Array}
         */
        function getSearchBoxChild() {
            if (searchBoxChild === null) {
                searchBoxChild = $('#' + searchBoxId + ' div');
            }
            return searchBoxChild;
        }

        /**
         * Append search box children.
         * @param {Array} data
         */
        function setSearchBoxChild(data) {
            var html = ['<div id="' + searchBoxId + '">'];
            for (var i = 0; i < data.length; i++) {
                html.push('<div>' + data[i] + '</div>');
            }
            html.push('</div>');
            this.after(html.join(''));
        }

        /**
         *
         * @param {String} needle
         * @param {String} stack
         * @return {Boolean}
         */
        function isContains(needle, stack) {
            return (stack.toLowerCase().indexOf(needle.toLowerCase()) === 0);
        }

        /**
         * Search a word.
         * @param {Array} items
         * @param {String} word
         */
        function search(items, word) {
            for (var i = 0; i < items.length; i++) {
                if (isContains(word, $(items[i]).html()) === true) {
                    $(items[i]).show();
                } else {
                    $(items[i]).hide();
                }
            }
        }

        /**
         * input onKeyUp event
         */
        this.keyup(function () {
            search(getSearchBoxChild(), $(this).val());
        });

        setSearchBoxChild.call(this, aData);
    };
})(jQuery);