/**
 * bindSlave jQuery plugin.
 *
 * @category example
 * @author Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @license http://creativecommons.org/licenses/by-nc-nd/3.0/deed.hu Creative Commons
 */
(function ($) {
    $.fn.bindSlave = function (aSlaveEl) {
        var _this = this;

        /**
         * Set some oprion to select.
         * @param {Array} data
         */
        function setOptions(data) {
            var options = [];
            for (var i = 0; i < data.length; i++) {
                options.push('<option value="', data[i].id + '">' + data[i].label + '</option>');
            }
            $(this).html(options.join(''));
        }

        /**
         * Master onChange event
         */
        function onChange() {
            var url = 'city/' + $(this).val() + '.json';
            $.getJSON(url, function (data) {
                setOptions.call($(aSlaveEl), data);
            });
        }

        this.change(onChange);

        $.getJSON('country.json', function (data) {
            setOptions.call(_this, data);
            onChange.call(_this);
        });
    };
})(jQuery);