<?php

/**
 * Session handling.
 *
 * @category example
 * @author Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @license http://creativecommons.org/licenses/by-nc-nd/3.0/deed.hu Creative Commons
 */
class Session {
    /**
     * True if the session is started.
     * @var bool
     */
    protected static $_started = false;
    /**
     * True if the session is destroyed.
     * @var bool
     */
    protected static $_destroyed = false;

    /**
     * @constructor
     */
    protected function __construct() {
        //
    }

    /**
     * Return a session value.
     *
     * @param string $aName session variable name
     *
     * @return mixed
     */
    protected static function read($aName) {
        $value = null;

        if (self::hasValue($aName)) {
            $value = $_SESSION[$aName];
        }

        return $value;
    }

    /**
     * Write or rewrite a session variable.
     *
     * @param string $aName session variable name
     * @param mixed $aValue session variable value
     *
     * @return void
     */
    protected static function write($aName, $aValue) {
        $_SESSION[$aName] = $aValue;
    }

    /**
     * Check the session variable is exists.
     *
     * @param string $aName session variable name
     *
     * @return bool true if it is exists
     */
    public static function  hasValue($aName) {
        return array_key_exists($aName, $_SESSION);
    }

    /**
     * Get a session variable value.
     *
     * @param string $aName session variable name
     *
     * @return mixed|null null if it is not exists
     */
    public static function  get($aName) {
        return self::read($aName);
    }

    /**
     * Set a session variable value.
     *
     * @param string $aName session variable name
     * @param mixed $aValue session variable value
     *
     * @return void
     */
    public static function  set($aName, $aValue) {
        self::write($aName, $aValue);
    }

    /**
     * Start new or resume existing session.
     *
     * @param array [$aOptions]
     *
     * @return bool it returns true if a session was successfully started
     */
    public static function  start($aOptions = array()) {
        // FIXME other session settings
        if (self::$_started === false) {
            self::$_started = session_start();
        }
        return self::$_started;
    }

    /**
     * Destroys all data registered to a session.
     *
     * @return bool returns true  if a session was successfully destroyed
     */
    public static function  destroy() {
        if (self::$_started === true and self::$_destroyed === false) {
            self::$_destroyed = session_destroy();
            self::$_started = !self::$_destroyed;
        }
        return self::$_destroyed;
    }
}