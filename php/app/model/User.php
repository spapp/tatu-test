<?php
/**
 * User handling class.
 *
 * @category  example
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @license   http://creativecommons.org/licenses/by-nc-nd/3.0/deed.hu Creative Commons
 */
class User {
    /**
     * User record map.
     *
     * @var array
     */
    protected static $_map = array(
        'name' => array(
            'type' => 'string',
            'default' => ''
        ),
        'email' => array(
            'type' => 'string',
            'default' => ''
        ),
        'username' => array(
            'type' => 'string',
            'default' => ''
        ),
        'password' => array(
            'type' => 'string',
            'default' => ''
        )
    );
    /**
     * User data.
     *
     * @var array
     */
    protected $_data = null;
    /**
     * Primery key(s).
     *
     * @var array
     */
    protected static $_primary = array(
        'name',
        'email'
    );

    /**
     * @param array $aData user data
     *
     * @constructor
     */
    public function __construct($aData = array()) {
        $this->_data = self::prepareData($aData);
    }

    /**
     * Prepare user data.
     *
     * @param array $aData
     * @param bool $aIsNew
     *
     * @return array prepared user data
     */
    protected static function prepareData($aData, $aIsNew = false) {
        $data = array();

        foreach (self::$_map as $fieldName => $fieldConfig) {
            if (array_key_exists($fieldName, $aData) and gettype($aData[$fieldName]) === $fieldConfig['type']) {
                $data[$fieldName] = $aData[$fieldName];
            } elseif ($aIsNew === true) {
                $data[$fieldName] = $fieldConfig['default'];
            }
        }

        return $data;
    }

    /**
     * Get data as array.
     */
    public function toArray() {
        $data = array();

        foreach(self::$_map as $fieldName => $fieldConfig){
            if(isset($this->{$fieldName})){
                $data[$fieldName] = $this->{$fieldName};
            }
        }

        return $data;
    }

    /**
     * Save the user data.
     */
    public function save() {
        // FIXME sql and other code come here
    }

    /**
     * Delete user.
     */
    public function remove() {
        // FIXME sql and other code come here
    }

    /**
     * Get a user property.
     *
     * @param string $aName
     *
     * @return null|mixed
     */
    public function __get($aName) {
        if (array_key_exists($aName, $this->_data)) {
            return $this->_data[$aName];
        }
        return null;
    }

    /**
     * Set a user prperty.
     *
     * @param string $aName
     * @param mixed $aValue
     *
     * @throws Exception
     */
    public function __set($aName, $aValue) {
        if (array_key_exists($aName, self::$_map) and gettype($aValue) === self::$_map[$aName]['type']) {
            $this->_data[$aName] = $aValue;
        } else {
            throw new Exception();
        }
    }

    /**
     * @param string $aName
     *
     * @return bool
     */
    public function __isset($aName){
        return isset($this->_data[$aName]);
    }

    /**
     * Create a User object.
     *
     * @param array $aData
     *
     * @static
     * @return User
     */
    public static function create($aData = array()) {
        return new self(self::prepareData($aData, true));
    }
}
