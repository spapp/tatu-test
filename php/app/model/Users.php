<?php

/**
 * Users handling class.
 *
 * @category  example
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @license   http://creativecommons.org/licenses/by-nc-nd/3.0/deed.hu Creative Commons
 */
class Users implements Countable, Iterator {
    /**
     * Query resource.
     *
     * @var resource
     */
    protected $_resource = null;
    /**
     * Record count.
     *
     * @var int
     */
    protected $_count = null;
    /**
     * Current record index.
     *
     * @var int
     */
    protected $_index = 0;

    /**
     * Constructor.
     *
     * @param resource $aDbResource
     */
    public function __construct($aDbResource) {
        $this->_resource = $aDbResource;
    }

    /**
     * Defined by Iterator interface
     */
    public function current() {
        $current = mysql_fetch_assoc($this->_resource);
        return new User($current);
    }

    /**
     * Defined by Iterator interface
     */
    public function key() {
        return $this->_index;
    }

    /**
     * Defined by Iterator interface
     */
    public function next() {
        ++$this->_index;
    }

    /**
     * Defined by Iterator interface
     */
    public function rewind() {
        $this->_index = 0;
        mysql_data_seek($this->_resource, $this->_index);
    }

    /**
     * Defined by Iterator interface
     */
    public function valid() {
        return $this->count() > $this->_index;
    }

    /**
     * Data in XML.
     *
     * @param string [$aEncoding]
     *
     * @return string
     */
    public function toXml($aEncoding = 'UTF-8') {
        $xml = array(
            '<?xml version="1.0" encoding="' . $aEncoding . '"?>',
            '<users>'
        );

        foreach ($this as $user) {
            array_push($xml, '<user>');

            foreach ($user->toArray() as $key => $value) {
                array_push($xml, '<' . $key . '>', $value, '</' . $key . '>');
            }

            array_push($xml, '</user>');
        }

        array_push($xml, '</users>');

        return implode('', $xml);
    }

    /**
     * Defined by Countable interface
     */
    public function count() {
        if ($this->_count === null) {
            // FIXME sql and other code come here
            // TODO for example mysql:
            $this->_count = mysql_num_rows($this->_resource);
        }
        return $this->_count;
    }

    /**
     * Destructor.
     */
    public function __destruct() {
        mysql_free_result($this->_resource);
    }

    /**
     * Get a user record.
     *
     * @param mixed $aParams
     *
     * @return null|User
     */
    public static function fetchOne($aParams) {
        // FIXME sql and other code come here
        $dbResource = 0; // The result resource

        if ($dbResource) {
            new User(mysql_fetch_assoc($dbResource));
        }

        return null;
    }

    /**
     * Get some user data.
     *
     * @return Users
     */
    public static function fetchAll() {
        // FIXME sql and other code come here
        $dbResource = 0; // The result resource
        return new self($dbResource);
    }


}
