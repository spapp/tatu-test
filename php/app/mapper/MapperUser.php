<?php
/**
 * User mapper class.
 *
 * @category  example
 * @author    Sandor Papp <spapp@spappsite.hu>
 * @copyright Copyright (c) 2013, Sandor Papp
 * @license   http://creativecommons.org/licenses/by-nc-nd/3.0/deed.hu Creative Commons
 */
class MapperUser {
    /**
     * Add a new user.
     *
     * @param array $aData user data
     *
     * @return int new user id
     */
    public static function addUser($aData) {
        $user = User::create($aData);
        return $user->save();
    }

    /**
     * Get a user.
     *
     * @param array $aParams query params
     *
     * @return null|User
     */
    public static function getUser($aParams) {
        // FIXME other code come here
        $params = ''; // prepared $aParams;
        return Users::fetchOne($params);
    }

    /**
     * Get some|all user.
     *
     * @return Users
     */
    public static function getUsers() {
        return Users::fetchAll();
    }

    /**
     * Login a user.
     *
     * @param string $aUserName
     * @param string $aPassword
     *
     * @return bool
     */
    public static function login($aUserName, $aPassword) {
        $user = self::getUser(array(
                                  'username' => $aUserName,
                                  'password' => $aPassword
                              ));

        if($user !== null){
            Session::set('userData', $user->toArray());
            return true;
        }
        return false;
    }
}